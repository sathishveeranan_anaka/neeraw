import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { Geolocation } from "@ionic-native/geolocation";
import { MyApp } from "./app.component";
import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from "angularfire2/database-deprecated";
import { AngularFireAuthModule } from "angularfire2/auth";
import { Facebook } from "@ionic-native/facebook";
import { GooglePlus } from "@ionic-native/google-plus";
import { AuthData } from "../providers/auth-data";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { HttpModule } from "@angular/http";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import * as ionicGalleryModal from "ionic-gallery-modal";
import { HAMMER_GESTURE_CONFIG } from "@angular/platform-browser";
import { IonicStorageModule } from "@ionic/storage";
import { HelperProvider } from "../providers/helper/helper";
import { DbProvider } from "../providers/db/db.service";
import { NetworkProvider } from "../providers/network/network.service";
import { Network } from "@ionic-native/network";
import * as firebase from "firebase";
import { NativeGeocoder } from "@ionic-native/native-geocoder";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { Camera } from "@ionic-native/camera";
import { Sim } from '@ionic-native/sim';
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, "assets/i18n/", ".json");
}

export const config = {
  apiKey: "AIzaSyBUiNQ-obDIMok3b5j3PII0t3a32luJF6w",
  authDomain: "neeraw-testing.firebaseapp.com",
  databaseURL: "https://neeraw-testing.firebaseio.com",
  projectId: "neeraw-testing",
  storageBucket: "neeraw-testing.appspot.com",
  messagingSenderId: "40487883967"
};

firebase.initializeApp(config);

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ionicGalleryModal.GalleryModalModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule.enablePersistence(),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    NativeGeocoder,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: ionicGalleryModal.GalleryModalHammerConfig
    },
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthData,
    Facebook,
    BarcodeScanner,
    Facebook,
    GooglePlus,
    Camera,
    DbProvider,
    HelperProvider,
    NetworkProvider,
    Network,
    Sim
  ]
})
export class AppModule { }
