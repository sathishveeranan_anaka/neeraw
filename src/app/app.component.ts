import { Component, ViewChild } from "@angular/core";
import {
  Nav,
  Platform,
  LoadingController,
  ToastController
} from "ionic-angular";

//***********  ionic-native **************/
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { NetworkProvider } from "../providers/network/network.service";
import { AuthData } from "../providers/auth-data";
import { TranslateService } from "@ngx-translate/core";
declare var navigator: any;
declare var Connection: any;

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav)
  nav: Nav;
  rootPage: string;
  menu: Array<any> = [];
  pages: Array<any>;
  public counter = 0;
  translations: any;
  constructor(
    public platform: Platform,
    public networkProvider: NetworkProvider,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public loadingCtrl: LoadingController,
    public authData: AuthData,
    public toastCtrl: ToastController,
    private translate: TranslateService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (window["FirebasePlugin"]) {
        var networkState = navigator.connection.type;
        if (networkState == Connection.NONE) {
          localStorage.setItem("online", "N");
        } else {
          localStorage.setItem("online", "Y");
        }
      } else {
        localStorage.setItem("online", "Y");
      }
      this.networkProvider.initializeNetworkEvents();

      this.translate.addLangs(["en", "hi", "ka"]);
      const language = localStorage.getItem("language");
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.translate.setDefaultLang("en");
      // if (language == undefined || language == null) {
      //   this.translate.setDefaultLang("en");
      //   this.rootPage = "SelectLangPage";
      // } else {
      this.translate.use("en");
      this.checkAuth();
      // }
      this.translate.get(["press_again_to_exit"]).subscribe(translations => {
        this.translations = translations;
      });
      this.platform.registerBackButtonAction(() => {
        if (this.counter == 0) {
          this.counter++;
          this.presentToast();
          setTimeout(() => {
            this.counter = 0;
          }, 3000);
        } else {
          this.platform.exitApp();
        }
      }, 0);
    });
  }

  toggleDetails(menu) {
    if (menu.showDetails) {
      menu.showDetails = false;
      menu.icon = "ios-add-outline";
    } else {
      menu.showDetails = true;
      menu.icon = "ios-remove-outline";
    }
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: this.translations.press_again_to_exit,
      duration: 3000,
      position: "top"
    });
    toast.present();
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  checkAuth() {
    let loadingPopup = this.loadingCtrl.create({
      spinner: "crescent",
      content: ""
    });
    loadingPopup.present();
    let that = this;
    this.authData.getCurrentUser().then(function (hasLoggedIn) {
      loadingPopup.dismiss();
      if (hasLoggedIn) {
        that.rootPage = "MenuPage";
      } else {
        that.rootPage = "LoginPage";
      }
    });
  }
}
