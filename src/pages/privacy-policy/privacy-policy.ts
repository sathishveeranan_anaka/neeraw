import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  ViewController
} from "ionic-angular";

/**
 * Generated class for the PrivacyPolicyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-privacy-policy",
  templateUrl: "privacy-policy.html"
})
export class PrivacyPolicyPage {
  unregisterBackButtonAction: any;
  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public view: ViewController,
    public navParams: NavParams
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad PrivacyPolicyPage");
  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }
  initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.view.dismiss();
      }
    ); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }

  ionViewDidLeave() {
    // this.userSubscriber.unsubscribe();
    // this.orderSubscriber.unsubscribe();
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
}
