import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  LoadingController,
  Platform
} from "ionic-angular";
import { NgStyle } from "@angular/common";

@IonicPage()
@Component({
  selector: "page-pop-over",
  templateUrl: "pop-over.html"
})
export class PopOverPage {
  store: any;
  total_amount: any;
  start_date: any;
  end_date: any;
  unregisterBackButtonAction: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public platform: Platform
  ) {
    this.store = this.navParams.get("data");
    this.total_amount = this.store.product.price * this.store.quantity;
    this.start_date = this.getDateFormat(this.store.start_date);
    this.end_date = this.getDateFormat(this.store.end_date);
    console.log(this.store)
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad PopOverPage");
  }

  getDateFormat(timestamp) {
    var timestampformat = new Date(timestamp);
    var date = timestampformat.getDate() + "/" + (timestampformat.getMonth() + 1) + "/" + timestampformat.getFullYear();
    return date;
  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.viewCtrl.dismiss();
      }
    );
  }

  ionViewDidLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  subscribeNow() {
    this.viewCtrl.dismiss("success");
  }

  cancel() {
    this.viewCtrl.dismiss("cancel");
  }
}
