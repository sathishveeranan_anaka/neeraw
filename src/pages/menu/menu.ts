import { Component, ViewChild } from "@angular/core";
import { IonicPage, NavController, NavParams, Nav } from "ionic-angular";
import { AuthData } from "../../providers/auth-data";
import { TranslateService } from "@ngx-translate/core";

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

export interface PageInterface {
  title: string;
  pageName: string;
  tabComponent?: any;
  index?: number;
  icon: string;
}

@IonicPage()
@Component({
  selector: "page-menu",
  templateUrl: "menu.html"
})
export class MenuPage {
  rootPage = "HomePage"; // what is difference between double quote and single quote in lazy loading.

  @ViewChild(Nav)
  nav: Nav;
  currentUser: {
    uid?: any;
    name?: any;
    location?: any;
    phoneNumber?: any;
  } = {};
  pages: PageInterface[] = [
    { title: "my_subscriptions", pageName: "MySubscriptionPage", icon: "cart", index: 3 },
    { title: "settings", pageName: "SettingsPage", icon: "settings", index: 0 },
    {
      title: "privacy_policy",
      pageName: "PrivacyPolicyPage",
      icon: "document",
      index: 1
    },
    { title: "logout", pageName: "LoginPage", icon: "power", index: 2 }
  ];
  menuNames = ["my_subscriptions", "settings", "privacy_policy", "logout"];
  constructor(
    public navCtrl: NavController,
    public authData: AuthData,
    public navParams: NavParams,
    private translate: TranslateService
  ) { }

  ionViewDidLoad() {
    this.currentUser.uid = "";
    this.authData.getCurrentUser().then(users => {
      if (users) {
        this.currentUser.phoneNumber = users.code + users.mobile_number;
        this.currentUser.name = users.name;
      } else {
        console.log("notlogin user===");
      }
    });
  }

  ionViewWillEnter() {
    let i;
    for (i = 0; i < this.menuNames.length; i += 1) {
      const index = i;
      this.translate.get(this.menuNames[i]).subscribe(res => {
        this.pages[index].title = res;
      });
    }
  }

  openPage(page: PageInterface) {
    if (page.index == 2) {
      this.authData.logoutUser();
      this.authData.clearCurrentuser();
      this.navCtrl.setRoot(page.pageName);
    } else {
      this.navCtrl.push(page.pageName);
    }
  }

  isActive(page: PageInterface) {
    let childNav = this.nav.getActiveChildNav();
    if (childNav) {
      if (
        childNav.getSelected() &&
        childNav.getSelected().root === page.tabComponent
      ) {
        return "primary";
      }
      return;
    }

    if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
      return "primary";
    }
  }
}
