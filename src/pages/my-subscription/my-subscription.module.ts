import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { MySubscriptionPage } from "./my-subscription";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [MySubscriptionPage],
  imports: [IonicPageModule.forChild(MySubscriptionPage), TranslateModule.forChild()]
})
export class MySubscriptionPageModule { }
