import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  Platform,
  ViewController,
  AlertController
} from "ionic-angular";
import { DbProvider } from "../../providers/db/db.service";
import { AuthData } from "../../providers/auth-data";
import { TranslateService } from "@ngx-translate/core";

@IonicPage()
@Component({
  selector: "page-my-subscription",
  templateUrl: "my-subscription.html"
})

export class MySubscriptionPage {
  center: any;
  public rows: any[] = [];
  public temp: any;
  tempRows: any[] = [];
  all_users: any;
  userId: any;
  driverUrl: any;
  userSubscriber: any;
  orderSubscriber: any;
  Isloading: boolean = true;
  unregisterBackButtonAction: any;
  translations: any;
  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public db: DbProvider,
    public authData: AuthData,
    public platform: Platform,
    public view: ViewController,
    public alertCtrl: AlertController,
    private translate: TranslateService
  ) { }

  ionViewDidLoad() {
    this.Isloading = true;
    this.translate.get(["please_wait"]).subscribe(translations => {
      this.translations = translations;
    });
    this.authData.getCurrentUser().then(hasLoggedIn => {
      if (hasLoggedIn) {
        this.userId = this.db.getUserUrl(hasLoggedIn.mobile_number);
        this.getOrders();
        this.Isloading = false;
      }
    });
  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => { this.view.dismiss(); });
  }

  ionViewDidLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  pauseSubscription(row, value) {
    let loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loading.present();
    var data = row;
    data.paused = value;
    this.db.updateSubscription(row.id, data).then(order => {
      if (value == true) {
        this.alert("Subscription is paused");
      } else {
        this.alert("Subscription is resumed");
      }

      loading.dismiss();
    });
  }

  getOrders() {
    var self = this;
    let loader = self.loadingCtrl.create({
      content: this.translations.please_wait
    });

    loader.present();
    this.orderSubscriber = this.db
      .getMySubscriptions(this.userId)
      .subscribe(orders => {
        this.rows = orders;
        loader.dismiss();
        this.Isloading = false;
      });
  }

  alert(msg) {
    let alert = this.alertCtrl.create({
      title: this.translations.alert,
      message: msg,
      enableBackdropDismiss: false,
      buttons: [
        {
          text: "Ok",
          handler: () => { }
        }
      ]
    });
    alert.present();
  }

}
