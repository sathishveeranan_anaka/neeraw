import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  Platform
} from "ionic-angular";
import { DbProvider } from "../../providers/db/db.service";

@IonicPage()
@Component({
  selector: "page-location-search-modal",
  templateUrl: "location-search-modal.html"
})
export class LocationSearchModalPage {
  code: string;
  locationData: any;
  listData: any = [];
  selectedLocation: string;
  tempList: any = [];
  unregisterBackButtonAction: any;
  constructor(
    public view: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public db: DbProvider
  ) { }

  ionViewDidLoad() {
    this.code = this.navParams.data.code;
    this.locationData = this.db.getLocationStorage();
    this.getTypeLocation();
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.tempList.filter(function (d) {
      return d["location"].toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.listData = temp;
  }

  getTypeLocation() {
    this.listData = [];
    for (let data in this.locationData) {
      var checked_status;
      var store: any = this.locationData[data];
      if (data == this.code) {
        checked_status = true;
      } else {
        checked_status = false;
      }
      let list = {
        location: store.location,
        pincode: store.pincode,
        checked: checked_status
      };
      this.listData.push(list);
    }
    this.tempList = this.listData;
  }

  selectLocation(data) {
    this.view.dismiss(data);
  }

  closeModal() {
    this.view.dismiss();
  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.view.dismiss();
      }
    );
  }

  ionViewDidLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
}
