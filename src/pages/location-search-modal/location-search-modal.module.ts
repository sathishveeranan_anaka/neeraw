import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { TranslateModule } from "@ngx-translate/core";
import { LocationSearchModalPage } from "./location-search-modal";

@NgModule({
  declarations: [LocationSearchModalPage],
  imports: [
    IonicPageModule.forChild(LocationSearchModalPage),
    TranslateModule.forChild()
  ]
})
export class LocationSearchModalPageModule {}
