import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { HomeDetailPage } from "./home-detail";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [HomeDetailPage],
  imports: [
    IonicPageModule.forChild(HomeDetailPage),
    TranslateModule.forChild()
  ]
})
export class HomeDetailPageModule { }
