import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  ViewController,
  AlertController,
  LoadingController,
  ModalController
} from "ionic-angular";
import { DbProvider } from "../../providers/db/db.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthData } from "../../providers/auth-data";

@IonicPage()
@Component({
  selector: "page-home-detail",
  templateUrl: "home-detail.html"
})
export class HomeDetailPage {
  unregisterBackButtonAction: any;
  id: any;
  language: any;
  translations: any;
  quantity: any;
  product: any;
  subscription_period: any;
  delivery_frequent: any;
  delivery_time: any;
  minDate: any;
  userId: any;
  constructor(
    public navCtrl: NavController,
    public db: DbProvider,
    public navParams: NavParams,
    public platform: Platform,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private translate: TranslateService,
    public view: ViewController,
    public authData: AuthData,
    public modalController: ModalController
  ) {
    this.language = localStorage.getItem("language");
    this.quantity = 1;
    this.delivery_frequent = "";
    this.subscription_period = "";
    this.delivery_time = "";
    this.product = [];
    var today = new Date();
    today.setDate(new Date().getDate() + 1);
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    this.minDate = yyyy + '-' + mm + '-' + dd

    this.translate
      .get([
        "please_wait",
        "NO_AMOUNT_TO_CENTER",
        "select_payment",
        "please_select_payment",
        "amount_settled",
        "Incorrect_code",
        "ok",
        "alert",
        "your_order_has_been_confirmed",
        "cancel"
      ])
      .subscribe(translations => {
        this.translations = translations;
      });
    this.id = this.navParams.get("id");
    this.authData.getCurrentUser().then(hasLoggedIn => {
      if (hasLoggedIn) {
        this.userId = this.db.getUserUrl(hasLoggedIn.mobile_number);
      }
    });

    this.db.getProductById(this.id).subscribe(productDetails => {
      let data: any = productDetails;
      this.product = data;
    });
  }

  alert(title) {
    let alert = this.alertCtrl.create({
      title: this.translations.alert,
      subTitle: title,
      buttons: [
        {
          text: this.translations.ok,
          handler: () => { }
        }
      ]
    });
    alert.present();
  }

  decreaseQty() {
    if (this.quantity - 1 > 0) {
      this.quantity = Number(this.quantity) - 1;
    }
  }

  increaseQty() {
    this.quantity = Number(this.quantity) + 1;
  }


  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.view.dismiss();
      }
    );
  }

  ionViewDidLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  pad(d) {
    return d < 10 ? "0" + d.toString() : d.toString();
  }

  close() {
    this.view.dismiss();
  }

  subscribe() {
    var tomorrow = new Date();
    tomorrow.setDate(new Date().getDate() + 1);
    var endDate = new Date();

    if (this.subscription_period == '1 month') {
      endDate.setMonth(new Date().getMonth() + 1);
    } else if (this.subscription_period == '3 months') {
      endDate.setMonth(new Date().getMonth() + 3);
    } else if (this.subscription_period == '6 months') {
      endDate.setMonth(new Date().getMonth() + 6);
    } else if (this.subscription_period == '12 months') {
      endDate.setMonth(new Date().getMonth() + 12);
    }

    var data = {
      quantity: this.quantity,
      delivery_frequent: this.delivery_frequent,
      subscription_period: this.subscription_period,
      delivery_time: this.delivery_time,
      product: this.product,
      user: this.userId,
      start_date: tomorrow.getTime(),
      end_date: endDate.getTime(),
      paused: false
    };
    let modal = this.modalController.create("PopOverPage", {
      data: data
    }, { cssClass: 'select-modal' });
    modal.present();
    modal.onDidDismiss(status => {
      if (status == "success") {
        this.db.addSubscription(new Date().getTime() + "", data).then(order => {
          this.alert(this.translations.your_order_has_been_confirmed);
          this.view.dismiss();
        });
      }
    });
  }
}
