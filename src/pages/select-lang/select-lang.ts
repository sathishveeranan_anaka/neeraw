import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  ViewController
} from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";

/**
 * Generated class for the SelectLangPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-select-lang",
  templateUrl: "select-lang.html"
})
export class SelectLangPage {
  unregisterBackButtonAction: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private translate: TranslateService,
    public platform: Platform,
    public view: ViewController
  ) { }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SelectLangPage");
  }

  setLangAs(lang: string) {
    switch (lang) {
      case "en":
        this.translate.use("en");
        break;
      case "hi":
        this.translate.use("hi");
        break;
      case "ka":
        this.translate.use("ka");
        break;
      case "sp":
        this.translate.use("sp");
        break;
      default:
        break;
    }
    localStorage.setItem("language", lang);
    this.navCtrl.setRoot("LoginPage");
  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.view.dismiss();
      }
    ); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }

  ionViewDidLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
}
