import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ModalController
} from "ionic-angular";
import { AuthData } from "../../providers/auth-data";
import { DbProvider } from "../../providers/db/db.service";
import { HelperProvider } from "../../providers/helper/helper";
import { AngularFireAuth } from "angularfire2/auth";
import firebase from "firebase";
import { TranslateService } from "@ngx-translate/core";
/**
 * Generated class for the LoginDriverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  public recaptchaVerifier: firebase.auth.RecaptchaVerifier;
  login_id: any;
  password: any;
  translations: any;
  rows: any;
  code: any;
  constructor(
    public navCtrl: NavController,
    public authData: AuthData,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public afsAuth: AngularFireAuth,
    public db: DbProvider,
    public helper: HelperProvider,
    public translate: TranslateService,
    public modalCtrl: ModalController,
  ) {
    this.login_id = "";
    this.password = "";
    this.code = "+91";
    this.translate
      .get([
        "mobile_number_required",
        "mobile_number_invalid",
        "enter_the_confirmation_code",
        "confirmation_code",
        "invalid_code",
        "successfully_logged_in",
        "mobile_number_not_registered",
        "no_internet_connection",
        "login_with_drivers_credentials",
        "please_wait",
        "select_country_code",
        "Cancel",
        "Verify",
        "ok"
      ])
      .subscribe(translations => {
        this.translations = translations;
      });
  }

  ionViewDidLoad() {
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
      "recaptcha-container"
    );
  }

  login() {
    let online_status = localStorage.getItem("online");
    if (online_status == "Y") {
      let phoneNumberString: any;
      if (this.code) {
        phoneNumberString = this.code + this.login_id;
        if (this.login_id == "") {
          this.helper.showMsg(this.translations.mobile_number_required, "");
        } else if (this.login_id.length != 10 && this.code == "+91") {
          this.helper.showMsg(this.translations.mobile_number_invalid, "");
        } else {
          let loading = this.loadingCtrl.create({
            content: this.translations.please_wait
          });
          loading.present();
          let subscriber = this.db.checkUser(this.login_id).subscribe(user => {
            if (user.length != 0) {
              subscriber.unsubscribe();
              let that = this;
              if (window["FirebasePlugin"]) {
                window["FirebasePlugin"].verifyPhoneNumber(
                  phoneNumberString, 30,
                  function (credential) {
                    var verificationId = credential.verificationId;
                    that.alertConfirm(verificationId);
                    loading.dismiss();
                  },
                  function (error) {
                    that.helper.showMsg(error, "");
                    loading.dismiss();
                  }
                );
              } else {
                loading.dismiss();
                that.helper.showMsg("cordova plugin not supported", "");
                const appVerifier = this.recaptchaVerifier;
                this.afsAuth.auth
                  .signInWithPhoneNumber(phoneNumberString, appVerifier)
                  .then(confirmationResult => {
                    let prompt = this.alertCtrl.create({
                      title: this.translations.enter_the_confirmation_code,
                      inputs: [
                        {
                          name: "confirmationCode",
                          placeholder: this.translations.confirmation_code,
                          type: "number"
                        }
                      ],
                      buttons: [
                        {
                          text: this.translations.Cancel,
                          handler: data => {
                            console.log("Cancel clicked");
                          }
                        },
                        {
                          text: this.translations.Verify,
                          handler: data => {
                            confirmationResult
                              .confirm(data.confirmationCode)
                              .then(result => {
                                this.getUser(result.user);
                              })
                              .catch(error => {
                              });
                          }
                        }
                      ]
                    });
                    prompt.present();
                  })
                  .catch(function (error) {
                    console.error("SMS not sent", error);
                  });
              }
            } else {
              subscriber.unsubscribe();
              loading.dismiss();
              this.helper.showMsg(
                this.translations.mobile_number_not_registered,
                ""
              );
            }
          });
        }
      } else {
        this.helper.showMsg("Select country code", "");
      }
    } else {
      this.helper.showMsg(this.translations.no_internet_connection, "");
    }
  }

  alertConfirm(verificationId) {
    let that = this;
    let prompt = this.alertCtrl.create({
      title: this.translations.enter_the_confirmation_code,
      inputs: [
        {
          name: "confirmationCode",
          placeholder: this.translations.confirmation_code,
          type: "number"
        }
      ],
      buttons: [
        {
          text: this.translations.Cancel,
          handler: data => {
            console.log("Cancel clicked");
          }
        },
        {
          text: this.translations.Verify,
          handler: data => {
            let loading = this.loadingCtrl.create({
              content: this.translations.please_wait
            });
            loading.present();
            var credential = firebase.auth.PhoneAuthProvider.credential(
              verificationId,
              data.confirmationCode
            );
            console.log("credential", credential);
            that.afsAuth.auth
              .signInWithCredential(credential)
              .then(success => {
                loading.dismiss();
                this.getUser(success);
              })
              .catch(error => {
                loading.dismiss();
                that.helper.showMsg(this.translations.invalid_code, "");
                console.log("Firebase failure: " + JSON.stringify(error));
              });
          }
        }
      ]
    });
    prompt.present();
  }

  getUser(user) {
    var subscribe = this.db.getUserDetails(this.login_id).subscribe(users => {
      subscribe.unsubscribe;
      let userdata: any = users;
      if (userdata.role == "user") {
        let jsonData = {
          name: userdata.name,
          address: userdata.address,
          uid: this.login_id,
          mobile_number: this.login_id,
          role: "user",
          code: "+91",
          location: userdata.location,
          pincode: userdata.pincode,
          timestamp: userdata.timestamp,
        };
        var self = this;
        this.authData.setCurrentUser(jsonData);
        this.helper.showMsg(this.translations.successfully_logged_in, "");
        this.authData.getCurrentUser().then(function (hasLoggedIn) {
          self.navCtrl.setRoot("MenuPage");
        }
        );
      } else {
        this.helper.showMsg(
          this.translations.login_with_drivers_credentials, "");
      }
    });
  }

  register() {
    this.navCtrl.setRoot("RegisterPage");
  }

  presentAlert(title) {
    let alert = this.alertCtrl.create({
      title: title,
      buttons: [this.translations.ok]
    });
    alert.present();
  }
}
