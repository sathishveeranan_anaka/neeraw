import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  ViewController
} from "ionic-angular";
import { HelperProvider } from "../../providers/helper/helper";
import { TranslateService } from "@ngx-translate/core";

@IonicPage()
@Component({
  selector: "page-changelanguage",
  templateUrl: "changelanguage.html"
})
export class ChangelanguagePage {
  selectLanguage: any;
  translations: any;
  unregisterBackButtonAction: any;
  constructor(
    public helper: HelperProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private translate: TranslateService,
    public platform: Platform,
    public view: ViewController
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad ChangelanguagePage");
    this.selectLanguage = localStorage.getItem("language");
    console.log("this.selectLanguage===", this.selectLanguage);
  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.view.dismiss();
      }
    ); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }

  ionViewDidLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  changeLanguage() {
    this.translate.setDefaultLang(this.selectLanguage);
    this.translate.use(this.selectLanguage);
    localStorage.setItem("language", this.selectLanguage);
    this.translate
      .get(["language_changed_successfully"])
      .subscribe(translations => {
        this.translations = translations;
        this.helper.showMsg(
          this.translations.language_changed_successfully,
          "success"
        );
      });

    this.navCtrl.setRoot("MenuPage");
  }
}
