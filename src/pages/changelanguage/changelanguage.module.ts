import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ChangelanguagePage } from "./changelanguage";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HttpModule } from "@angular/http";
import { HttpClient, HttpClientModule } from "@angular/common/http";
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, "assets/i18n/", ".json");
}

@NgModule({
  declarations: [ChangelanguagePage],
  imports: [
    HttpModule,
    HttpClientModule,
    IonicPageModule.forChild(ChangelanguagePage),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ]
})
export class ChangelanguagePageModule {}
