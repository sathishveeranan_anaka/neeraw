import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ModalController } from "ionic-angular";
import { AuthData } from "../../providers/auth-data";
import { TranslateService } from "@ngx-translate/core";
import { DbProvider } from "../../providers/db/db.service";
import { AngularFirestore } from "angularfire2/firestore";

@IonicPage()
@Component({
  selector: "page-home",
  templateUrl: "home.html"
})

export class HomePage {
  products: any;
  name: any;
  banners: any;
  subscriptions: any;
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  constructor(
    public navCtrl: NavController,
    public authData: AuthData,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private afs: AngularFirestore,
    private translate: TranslateService,
    private db: DbProvider
  ) {
    this.name = "";
    this.products = [];
    this.banners = [];
    this.subscriptions = [];
  }

  ionViewDidLoad() {
    this.authData.getCurrentUser().then(users => {
      if (users) {
        this.name = users.name;
        this.getSubscriptions(this.db.getUserUrl(users.mobile_number));
        this.loadCenterInfo(users);
      }
      this.getBanners();
    });
  }

  getBanners() {
    this.db.getBanners().subscribe(banners => {
      this.banners = banners
    })
  }

  getSubscriptions(userId) {
    this.db
      .getMySubscriptions(userId)
      .subscribe(subscriptions => {
        this.subscriptions = subscriptions;
      });
  }

  loadCenterInfo(location) {
    this.db.getProductByPincode(location)
      .subscribe(product => {
        this.products = product;
      });
  }

  gotoProductDetail(product) {
    this.navCtrl.push("HomeDetailPage", { id: product.id });
  }
}
