import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  ViewController
} from "ionic-angular";

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-settings",
  templateUrl: "settings.html"
})
export class SettingsPage {
  public unregisterBackButtonAction: any;
  currentUser: {
    uid?: any;
    username?: any;
  } = {};

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    public view: ViewController
  ) {
    this.currentUser.uid = "";
  }

  ionViewDidLoad() {
    const language = localStorage.getItem("language");
    console.log("ionViewDidLoad SettingsPage " + language);
  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.view.dismiss();
      }
    ); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }

  ionViewDidLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  openchangemobileno() {
    this.navCtrl.push("ChangemobilenoPage");
  }

  openchangepassword() {
    this.navCtrl.push("ChangepasswordPage");
  }

  openchangelanguage() {
    this.navCtrl.push("ChangelanguagePage");
  }
}
