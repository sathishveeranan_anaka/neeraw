import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ModalController
} from "ionic-angular";
import { AuthData } from "../../providers/auth-data";
import { DbProvider } from "../../providers/db/db.service";
import { HelperProvider } from "../../providers/helper/helper";
import { AngularFireAuth } from "angularfire2/auth";
import firebase from "firebase";
import { TranslateService } from "@ngx-translate/core";
/**
 * Generated class for the LoginDriverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-register",
  templateUrl: "register.html"
})
export class RegisterPage {
  public recaptchaVerifier: firebase.auth.RecaptchaVerifier;
  name: any;
  address: any;
  mobile_number: any;
  translations: any;
  location: any;
  pincode: any;
  constructor(
    public navCtrl: NavController,
    public authData: AuthData,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public afsAuth: AngularFireAuth,
    public db: DbProvider,
    public helper: HelperProvider,
    public translate: TranslateService,
    public modalCtrl: ModalController,
  ) {
    this.name = "";
    this.location = "";
    this.mobile_number = "+91";
    this.pincode = 0;
    this.translate
      .get([
        "mobile_number_required",
        "mobile_number_invalid",
        "enter_the_confirmation_code",
        "confirmation_code",
        "invalid_code",
        "successfully_logged_in",
        "mobile_number_not_registered",
        "Mobile_number_is_already_registered",
        "no_internet_connection",
        "login_with_drivers_credentials",
        "please_wait",
        "select_country_code",
        "Cancel",
        "Verify",
        "ok"
      ])
      .subscribe(translations => {
        this.translations = translations;
      });
  }

  ionViewDidLoad() {
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier("recaptcha-container");
  }

  selectLocation() {
    const selectLocationModal = this.modalCtrl.create(
      "LocationSearchModalPage", {},
      {
        enableBackdropDismiss: false,
        cssClass: "modal-h80"
      }
    );
    selectLocationModal.present();
    selectLocationModal.onDidDismiss(data => {
      if (data && data.location != undefined) {
        this.location = data.location;
        this.pincode = data.pincode;
      }
    });
  }


  insertData(that, user, loading) {
    let registerdata = {
      name: this.name,
      address: this.address,
      mobile_number: this.mobile_number,
      code: "+91",
      location: this.location,
      pincode: this.pincode,
      timestamp: new Date().getTime(),
      role: "user",
    };
    this.db.registerUser(this.mobile_number, registerdata);
    this.authData.setCurrentUser(registerdata);
    this.helper.showMsg(this.translations.successfully_logged_in, "");
    loading.dismiss();
    this.navCtrl.setRoot("MenuPage");
  }

  back() {
    this.navCtrl.setRoot("LoginPage");
  }

  register() {
    let online_status = localStorage.getItem("online");
    if (online_status == "Y") {
      if (this.name == "") {
        this.helper.showMsg(this.translations.First_name_required, "");
      } else if (this.mobile_number == "") {
        this.helper.showMsg(this.translations.Mobile_number_required, "");
      } else if (this.mobile_number.length != 10) {
        this.helper.showMsg(this.translations.mobile_number_invalid, "");
      } else {
        const phoneNumberString = "+91" + this.mobile_number;
        let that = this;
        let loading = this.loadingCtrl.create({
          content: this.translations.please_wait
        });
        loading.present();
        let subscribe = this.db
          .checkUser(this.mobile_number)
          .subscribe(user => {
            subscribe.unsubscribe();
            if (user.length == 0) {
              this.registerProcess(loading, phoneNumberString);
            } else {
              loading.dismiss();
              this.helper.showMsg(
                this.translations.Mobile_number_is_already_registered, "");
            }
          });
      }
    } else {
      this.helper.showMsg(this.translations.no_internet_connection, "");
    }
  }

  registerProcess(loading, phoneNumberString) {
    if (window["FirebasePlugin"]) {
      window["FirebasePlugin"].verifyPhoneNumber(
        phoneNumberString,
        30,
        function (credential) {
          var verificationId = credential.verificationId;
          this.alertConfirm(verificationId);
          loading.dismiss();
        },
        function (error) {
          this.helper.showMsg(error, "");
          loading.dismiss();
        }
      );
    } else {
      loading.dismiss();
      this.helper.showMsg("cordova plugin not supported...", "");
      const appVerifier = this.recaptchaVerifier;
      this.afsAuth.auth
        .signInWithPhoneNumber(phoneNumberString, appVerifier)
        .then(confirmationResult => {
          let prompt = this.alertCtrl.create({
            title: this.translations.enter_the_confirmation_code,
            inputs: [
              {
                name: "confirmationCode",
                placeholder: this.translations.confirmation_code,
                type: "number"
              }
            ],
            buttons: [
              {
                text: this.translations.Cancel,
                handler: data => { }
              },
              {
                text: this.translations.Verify,
                handler: data => {
                  confirmationResult
                    .confirm(data.confirmationCode)
                    .then(result => {
                      let loading = this.loadingCtrl.create({
                        content: this.translations.please_wait
                      });
                      loading.present();
                      this.insertData(this, result.user, loading);
                    })
                    .catch(error => { });
                }
              }
            ]
          });
          prompt.present();
        })
        .catch(function (error) {
          console.error("SMS not sent", error);
        });
    }
  }

  alertConfirm(verificationId) {
    let that = this;
    let prompt = this.alertCtrl.create({
      title: this.translations.enter_the_confirmation_code,
      inputs: [
        {
          name: "confirmationCode",
          placeholder: this.translations.confirmation_code,
          type: "number"
        }
      ],
      buttons: [
        {
          text: this.translations.Cancel,
          handler: data => { }
        },
        {
          text: this.translations.Verify,
          handler: data => {
            let loading = this.loadingCtrl.create({
              content: this.translations.please_wait
            });
            loading.present();
            var credential = firebase.auth.PhoneAuthProvider.credential(
              verificationId,
              data.confirmationCode
            );
            that.afsAuth.auth
              .signInWithCredential(credential)
              .then(success => {
                this.insertData(this, success.user, loading);
              })
              .catch(error => {
                loading.dismiss();
                that.helper.showMsg(this.translations.invalid_code, "");
              });
          }
        }
      ]
    });
    prompt.present();
  }

  presentAlert(title) {
    let alert = this.alertCtrl.create({
      title: title,
      buttons: [this.translations.ok]
    });
    alert.present();
  }
}
