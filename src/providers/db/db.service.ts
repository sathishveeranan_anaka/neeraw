import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection
} from "angularfire2/firestore";
import { Storage } from "@ionic/storage";
import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";

export interface Item {
  name: string;
}

@Injectable()
export class DbProvider {
  public test = "test";
  allCountryList: any;
  private orderCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;
  locationList: any;

  constructor(public http: HttpClient,
    public afs: AngularFirestore, public storage: Storage) {
    this.orderCollection = this.afs.collection<Item>("orders");
    this.getLocationFromJson();
  }

  public getCountryFromJson() {
    return this.http.get("assets/country_code.json");
  }

  public getLocationFromJson() {
    this.http.get("assets/location.json").subscribe(data => {
      this.locationList = data;
    });
  }

  getLocationStorage() {
    return this.locationList;
  }

  registerUser(uid, userInfo) {
    this.afs.collection('users')
      .doc(uid)
      .set(userInfo)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  addSubscription(sid, data) {
    return this.afs.collection('subscriptions').doc(sid).set(data);
  }

  public getProductById(id) {
    let refUrl = "products/" + id;
    return this.afs.doc(refUrl).valueChanges();
  }

  public getProductByPincode(location) {
    return this.afs
      .collection("products", ref => ref.where("pincode", "==", location.pincode))
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
  }

  public getBanners() {
    return this.afs
      .collection("banners")
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
  }

  public getTodayOrders(center, driver) {
    var date = new Date();
    var month = ("0" + (date.getMonth() + 1)).slice(-2);
    var day = ("0" + date.getDate()).slice(-2);
    var year = date.getFullYear();
    var date_str = day + "-" + month + "-" + year;
    console.log("Date is " + date_str);
    return this.afs
      .collection("orders", ref =>
        ref
          .where("center", "==", center)
          .where("cart_product.date", "==", date_str)
          .where("cart_product.driver", "==", driver)
          .where("cart_product.priceby", "==", "hour")
      )
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  public getCurrentFutureOrders(center, driver) {
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();
    var date_timestamp = new Date(
      Date.UTC(year, month, day, 0, 0, 0, 0)
    ).getTime();

    return this.afs
      .collection("orders", ref =>
        ref
          .where("center", "==", center)
          .where("cart_product.date_timestamp", ">=", date_timestamp)
          .where("cart_product.driver", "==", driver)
          .where("cart_product.priceby", "==", "hour")
      )
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  public getUploadPictureOrders(center, driver) {
    return this.afs
      .collection("orders", ref =>
        ref
          .where("center", "==", center)
          .where("cart_product.driver", "==", driver)
          .where("cart_product.priceby", "==", "hour")
      )
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  public sendMessage(to, text) {
    var url = "http://www.myvaluefirst.com/smpp/sendsms?username=anakagrplthtp&password=aagrveht&to=" + to + "&from=KMNTRA&text=" + text + "&dlr-mask=19&dlr-url&coding=3"
    this.http.get(url).subscribe(data => {
      console.log(data);
    });
  }

  public getFutureOrders(center, driver) {
    var date = new Date();
    var last = new Date(date.getTime() + 1 * 24 * 60 * 60 * 1000);
    var day = last.getDate();
    var month = last.getMonth();
    var year = last.getFullYear();
    var date_timestamp = new Date(
      Date.UTC(year, month, day, 0, 0, 0, 0)
    ).getTime();
    return this.afs
      .collection("orders", ref =>
        ref
          .where("center", "==", center)
          .where("cart_product.driver", "==", driver)
          .where("cart_product.date_timestamp", ">=", date_timestamp)
          .where("cart_product.priceby", "==", "hour")
      )
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  public getMySubscriptions(user) {
    return this.afs
      .collection("subscriptions", ref => ref.where("user", "==", user))
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  public getEscalatedOrders(center, driver) {
    return this.afs
      .collection("orders", ref =>
        ref
          .where("center", "==", center)
          .where("cart_product.driver", "==", driver)
          .where("order_status", "==", "Order Escalated")
          .where("cart_product.priceby", "==", "hour")
          .orderBy("cart_product.date_timestamp")

      )
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }



  public getSelectOrders(center) {
    return this.afs
      .collection("orders", ref =>
        ref.where("center", "==", center).orderBy("timestamp")
      )
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  public getOrderById(id) {
    let refUrl = "orders/" + id;
    console.log("refUrl===", refUrl);
    return this.afs.doc(refUrl).valueChanges();
  }

  public updateOrderById(id, data) {
    let key = "orders/" + id;
    return this.afs.doc(key).update(data);
  }

  public getAllInventory() {
    return this.afs
      .collection("inventory")
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  public updatePayment(id, data) {
    let key = "payment/" + id;
    return this.afs.doc(key).update(data);
  }

  public updateSubscription(id, data) {
    let key = "subscriptions/" + id;
    return this.afs.doc(key).update(data);
  }

  public createOrder(data) {
    return this.orderCollection.add(data);
  }

  public getUserDetails(uid) {
    return this.afs.doc("/users/" + uid).valueChanges();
  }

  public getCategories() {
    return this.afs
      .collection("category")
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  public getSubCategories() {
    return this.afs
      .collection("sub_category")
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  public getDriverData() {
    return this.afs
      .collection("driver")
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  public getCentreUrl(id) {
    let refUrl = "center/" + id;
    return this.afs.doc(refUrl).ref;
  }

  public getUserUrl(id) {
    let refUrl = "users/" + id;
    return this.afs.doc(refUrl).ref;
  }

  public getCategoryUrl(id) {
    let refUrl = "category/" + id;
    return this.afs.doc(refUrl).ref;
  }

  public getVillageUrl(id) {
    let refUrl = "village/" + id;
    return this.afs.doc(refUrl).ref;
  }

  public updateUser(id, data) {
    let key = "users/" + id;
    return this.afs.doc(key).update(data);
  }

  public getCustomerId(id) {
    let refUrl = "users/" + id;
    return this.afs.doc(refUrl).valueChanges();
  }

  public getUserRef(id) {
    let refUrl = "users/" + id;
    return this.afs.doc(refUrl).ref;
  }

  public getUserList() {
    return this.afs
      .collection<Item>("users", ref => ref.orderBy("timestamp"))
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  public getUser(center) {
    return this.afs
      .collection<Item>("users", ref =>
        ref.where("center", "==", center).orderBy("timestamp")
      )
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  public getPaymentByOrderId(order_id) {
    return this.afs
      .collection<Item>("payment", ref =>
        ref.where("order_id", "==", order_id).orderBy("timestamp")
      )
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          console.log("ID" + id);
          return { id, ...data };
        });
      });
  }

  public getUserById(uid) {
    return this.afs.doc("/users/" + uid).valueChanges();
  }

  public getCenterById(id) {
    return this.afs.doc("/center/" + id).valueChanges();
  }

  public updateCenter(id, data) {
    let key = "center/" + id;
    return this.afs.doc(key).update(data);
  }

  public checkUser(mobilenumber) {
    return this.afs
      .collection("users", ref =>
        ref.where("mobile_number", "==", mobilenumber)
      )
      .valueChanges();
  }
}
