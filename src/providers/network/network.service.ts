import { Injectable } from "@angular/core";
import { AlertController, Events } from "ionic-angular";
import { Network } from "@ionic-native/network";

export enum ConnectionStatusEnum {
  Online,
  Offline
}

@Injectable()
export class NetworkProvider {
  previousStatus;
  constructor(
    public alertCtrl: AlertController,
    public network: Network,
    public eventCtrl: Events
  ) {
    console.log("Hello NetworkProvider Provider");
    this.previousStatus = ConnectionStatusEnum.Online;
  }

  public initializeNetworkEvents(): void {
    this.network.onDisconnect().subscribe(() => {
      localStorage.setItem("online", "N");
    });
    this.network.onConnect().subscribe(() => {
      localStorage.setItem("online", "Y");
    });
  }
}
